package jeesimple;

/**
 * Created by Admin on 12.02.2016.
 */
import org.picketlink.idm.credential.AbstractBaseCredentials;

/**
 * <p>A simple credential that uses a token as a credential.</p>
 */
public class SimpleTokenCredential extends AbstractBaseCredentials {

    private String token;

    public SimpleTokenCredential(String token) {
        this.token = token;
    }

    @Override
    public void invalidate() {
        this.token = null;
    }

    public String getToken() {
        return this.token;
    }
}
