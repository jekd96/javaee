package jeesimple;

/**
 * Created by Admin on 12.02.2016.
 */
import org.picketlink.annotations.PicketLink;
import org.picketlink.authentication.BaseAuthenticator;
import org.picketlink.credential.DefaultLoginCredentials;
import org.picketlink.idm.credential.Password;
import org.picketlink.idm.model.basic.User;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

/**
 * <p>A simple authenticator that supports two credential types: username/password or a simple token.</p>
 */
@RequestScoped
@PicketLink
public class CustomAuthenticator extends BaseAuthenticator {

    @Inject
    private DefaultLoginCredentials credentials;

    @Override
    public void authenticate() {
        if (this.credentials.getCredential() == null) {
            return;
        }

        if (isUsernamePasswordCredential()) {
            String userId = this.credentials.getUserId();
            Password password = (Password) this.credentials.getCredential();

            if (userId.equals("admin") && String.valueOf(password.getValue()).equals("admin")) {
                successfulAuthentication();
            }
        } else if (isCustomCredential()) {
            SimpleTokenCredential customCredential = (SimpleTokenCredential) this.credentials.getCredential();

            if (customCredential.getToken() != null && customCredential.getToken().equals("valid_token")) {
                successfulAuthentication();
            }
        }
    }

    private boolean isUsernamePasswordCredential() {
        return Password.class.equals(credentials.getCredential().getClass()) && credentials.getUserId() != null;
    }

    private boolean isCustomCredential() {
        return SimpleTokenCredential.class.equals(credentials.getCredential().getClass());
    }

    private User getDefaultUser() {
        return new User("admin");
    }

    private void successfulAuthentication() {
        setStatus(AuthenticationStatus.SUCCESS);
        setAccount(getDefaultUser());
    }

}
